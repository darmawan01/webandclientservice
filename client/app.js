// Config environment
const express = require("express"); // Import express
const axios = require("axios").default;

const app = express(); // Make express app

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.post("/", async (req, res) => {
  let name = req.body.name;

  const result = await axios({
    method: "post",
    url: "http://localhost:3000/",
    data: {
      name: name,
    },
  });
  console.log(result.data);
  res.json(result.data);
});

/* Run the server */
if (process.env.NODE_ENV || process.env.PORT !== "test") {
  app.listen(5000, () => console.log(`Server running on 5000`));
}

module.exports = app;

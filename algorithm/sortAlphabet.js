// 1
function reverseStr(str) {
  return str.split("").sort().join(""); // reverse those words
}

// 2
function sortStr(str) {
  const newStr = str.split("");
  for (let i = 0; i < newStr.length; i++) {
    for (let j = i; j < newStr.length; j++) {
      if (newStr[i] > newStr[j]) {
        let temp = newStr[i];
        newStr[i] = newStr[j];
        newStr[j] = temp;
      }
    }
  }
  return newStr.join("");
}

console.log(sortStr("omama"));
console.log(sortStr("osama"));
